parameter <- c(
  "alpha1_dt"			
  ,"mu_lgtq" 
  ,"mu_alphav"
  ,"mu_beta1v"
  ,"mu_beta2v"
)

latent.var <- c(
  "tau"
  ,"alpha2_dt" 	
  ,"alpha3_dt" 	
  ,"alpha_v"
  ,"beta1_v"
  ,"beta2_v"
  ,"u"         
  ,"N"        
)

variable <- c(parameter,latent.var)