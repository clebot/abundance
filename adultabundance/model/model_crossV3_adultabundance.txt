model							# Egg model
{

#############################################################################################################################################
# 1/ HIERARCHICAL MODELING OF PRECISION																										#
#############################################################################################################################################
## ntau : Total number of variability parameters																							#
## tau[1] : Precision of year effect on log density																							#
## tau[2] : Precision of river effect on log density																						#
## tau[3:5] : Precisions of exploitation rate intercepts (complementary log-log scale) on first, second and third fishing periods			#
## tau[6:8] : Precisions of flow coefficients on first, second and third fishing periods													#
## tau[9:11] : Precisions of fishing duration coefficients  on first, second and third fishing periods										#
## tau[12] : Precision of density (log scale)																								#
## tau[13] : Precision of 1SW proportion in the population (log scale)																		#
## tau[14:16] : Precisions of complementary log-log exploitation rate during the first, second and third fishing periods					#
#############################################################################################################################################

	# Hierarchical modeling of every precision 
	for (x in 1:ntau)
	{
	tau[x] ~ dgamma(0.1,0.1)
	}

#############################################################################################################################################
# 2/ ESTIMATION OF EGG DEPOSITION PER RIVER AND YEAR																						#
#	Characteristics : 	....																												#
#############################################################################################################################################
## t: year from 1 to nyear=31 (1987:2017)																									#
## r: river from 1 to nriver=18   1:Couesnon, 2:Leff, 3:Trieux, 4:Jaudy, 5:Léguer, 6:Yar, 7:Douron, 8:Queffleuth, 9:Penzé, 10:Elorn,		#
##								  11: Mignonne, 12:Aulne, 13:Goyen, 14:Odet, 15:Aven, 16:Elle-Isole, 17:Scorff and 18:Blavet				#
## a: sea age category from 1 (MSW individuals) to nage=2 (1SW individuals)																	#
## fp: fishing period from 1 to nfp=3																										#
#############################################################################################################################################

#####################################################################################################################################################################
## DATA																																								#
## -----------------------------------------------------------------------------------------------------------------------------------------------------------------#							
## Y[r,t,1:2]: Catch of MSW and 1SW individuals 																													#
##																																									#
## N[r,t,1:2]: Adult return observed per sea age category (only available for the Scorff,1994-2017 and the Elorn, 2007-2017)										#
## Nc[r,t,1:2] : Adult return partially observed per sea age category. Available only for the COUESNON (1996-2015). Used as censored data							#
## is.Nc[r,t,1:2] : Index matrix taken 1 if N[r,t,1:2] > or = Nc[r,t,1:2] and 0 otherwise. Used to model the censored data											#
## NC[r,t,1:2] : Adult return partially observed per sea age category (available for the Aulne, 1999-2017 and MSW individuals in the Elorn, 2007)					#
##																																									#
##
## E[r,t,1:3] : Duration of the first, second and third fishing periods (expressed in number of days)																# 
## Q[t,1:3] : Daily discharge averaged over rivers of Brittany during the first, second and third fishing periods (see the paper for more precision)				#
## SRR[r,t]: Surface of rivers expressed in riffle/rapid equivalent (unit:100m²) 																					#
#####################################################################################################################################################################

##############################
#        Parameters          #
##############################

# Adult returns
###############

## Density

# Mean of log density distribution
alpha1_dt ~ dnorm(0,0.01)

# Year effects on mean log density
for (t in 1:(nyear+1))
{
	alpha2_dt[t] ~ dnorm(0,tau[1])
}

# River effects on mean log density
for (r in 1:nriver) 
{
	alpha3_dt[r] ~ dnorm(0,tau[2])		
}	

# Mean proportion of 1SW in the population (logit scale)
mu_lgtq ~ dnorm(0,0.1)									


for (fp in 1:nfp)
{

## Catch per unit of effort

# Exploitation rate
###################

## Intercepts and coefficients of the linear model on complementary loglog exploitation rate

mu_alphav[fp] ~ dnorm(0,0.01)
mu_beta1v[fp] ~ dnorm(0,0.01)
mu_beta2v[fp] ~ dnorm(0,0.01)


###############################
#       Latent variables      #
###############################

for (r in 1:nriver) 
{
alpha_v[r,fp] ~ dnorm(mu_alphav[fp],tau[2+fp])			
beta1_v[r,fp] ~ dnorm(mu_beta1v[fp],tau[5+fp])			
beta2_v[r,fp] ~ dnorm(mu_beta2v[fp],tau[8+fp])			
}

}



for (t in 1:(nyear+1))	
{
	for (r in 1:nriver) 
	{
		
# Adult returns
###############

## Density 
# Total density. Hierarchical modeling on the log scale with year and river effects	
		lgmu_dt[r,t] <- alpha1_dt+alpha2_dt[t]+alpha3_dt[r]			
		lgdt[r,t] ~ dnorm(lgmu_dt[r,t],tau[12])					
		dt[r,t] <- exp(lgdt[r,t])										
		
# Proportion of 1SW in the population. Hierarchical modeling on the logit scale without neither year nor river effects	
		lgtq[r,t] ~ dnorm(mu_lgtq,tau[13])			
		q[r,t] <- ilogit(lgtq[r,t])
				
# Density per sea age category
		d[r,t+1,1] <- dt[r,t]*(1-q[r,t])			
		d[r,t,2] <- dt[r,t]*q[r,t]					 
	}
}

for (t in 1:nyear)												
{
	for (r in 1:nriver)
	{

# Exploitation rate
###################	
	
		for (fp in 1:nfp)
		{
## Complementary log-log scaled exploitation rates 
			v[r,t,fp] <- exp(lgv[r,t,fp])
			lgv[r,t,fp] ~ dnorm(mu_lgv[r,t,fp],tau[13+fp])
			mu_lgv[r,t,fp] <- (alpha_v[r,fp]+(log(Q[t,fp])-mu_lgQ[fp])*beta1_v[r,fp]+(log(E[r,t,fp])-mu_lgE[fp])*beta2_v[r,fp])*Iv[r,t,fp]
		}
		for (a in 1:nage)
		{
## Exploitation rates 
			u[r,t,a] <- 1-exp(-(sum(v[r,t,]*I[r,t,a,])))

# Adult returns
###############		

			n[r,t,a] <- ifelse(a==1,d[r,t+1,a]*SRR[r,t],d[r,t+1,a]*SRR[r,t+1]) 	
## Censored data from the Couesnon
			is.Nc[r,t,a] ~ dinterval(N[r,t,a], Nc[r,t,a])
			
## Abundance of adult returns
			N[r,t,a] ~ dpois(n[r,t,a])	
			
######################
#    Likelihood      #
######################

## Yield
			Y[r,t,a] ~ dbin (u[r,t,a],N[r,t,a])				# Yield 
			
		}
	}

}

######################
#    Specific case   #
######################

## The partial observations of adult returns in the Aulne and the Elorn (MSW, 2007)

# Partial observation in the Elorn (MSW, 2007)	
NC[10,(2007-1987+1),1] ~ dbin(p[10,(2007-1987+1),1],N[10,(2007-1987+1),1])
p[10,(2007-1987+1),1] ~ dunif(u[10,(2007-1987+1),1],1)

# Partial observation in the Aulne (MSW and 1SW, 1999:2017)	
for (t in (1999-1987+1):nyear) # from 1999 to 2017. Temporal interval corresponding to the time serie of returns collected in the Aulne
{
	for (a in 1:nage)
	{
		NC[12,t,a] ~ dbin(p[12,t,a],N[12,t,a])
	}
	p[12,t,1] ~ dunif(u[12,t,1],1)
	p[12,t,2] ~ dunif(p[12,t,1],1)
}

}

