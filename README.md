# Taking full advantage of the diverse assemblage of data at hand to produce time series of abundance: a case study on Atlantic salmon populations of Brittany

This gitlab page has been created with the aim of giving readers access to most of the materials used in the article:  
*Taking full advantage of the diverse assemblage of data at hand to produce time series of abundance: a case study on Atlantic salmon populations of Brittany*  
This article has been written by Lebot Clément <sup>1,2,3</sup>, Arago Marie-André <sup>4</sup>, Beaulaton Laurent <sup>3,5</sup>, Germis Gaëlle <sup>6</sup>, Nevoux Marie <sup>3,7</sup>, Rivot Etienne <sup>3,7</sup> and Prévost Etienne <sup>2,3</sup>; and submitted to the Canadian Journal of Fisheries and Aquatic Sciences.

<sup>1</sup> Université de Pau et des Pays de l'Adour  
<sup>2</sup> ECOBIOP, Behavioural Ecology and Fish Population Biology, INRAE, Université de Pau et des Pays de l'Adour  
<sup>3</sup> Management of Diadromous Fish in their Environment, OFB, INRAE, Institut Agro, Université de Pau et des Pays de l'Adour / e2s UPPA  
<sup>4</sup> DIR Bretagne-Pays de la Loire, OFB  
<sup>5</sup> Direction de la Recherche et de l’Appui Scientifique, OFB  
<sup>6</sup> Bretagne Grands Migrateurs  
<sup>7</sup> ESE, Ecology and Ecosystems Health, Institut Agro, INRAE  

## Abstract

Estimation of abundance with wide spatio-temporal coverage is essential to the assessment and management of wild populations. But, in many cases, data available to estimate abundance time series have diverse forms, variable quality over space and time and they stem from multiple data collection procedures. We developed a Hierarchical Bayesian Modelling (HBM) approach that take full advantage of the diverse assemblage of data at hand to estimate homogeneous time series of abundances. We apply our approach to the estimation of adult abundances of 18 Atlantic salmon populations of Brittany (France) from 1987 to 2017 using catch statistics, environmental covariates, fishing effort and abundance indices. Additional data of total or partial abundance collected in 4 more closely monitored populations are also integrated into the analysis. The HBM framework allows the transfer of information from the closely monitored populations to the others. Our results show a general pattern of abundance stability over the period studied. The apparent contradiction with the general claim of an overall decline of Atlantic salmon abundance at the scale of the species distribution range is discussed. 